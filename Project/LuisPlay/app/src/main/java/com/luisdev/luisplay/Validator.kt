package com.luisdev.luisplay

object Validator {

    /**
     * Query text in search input will be invalid if text is equals to empty
     */
    fun validateSearchQuery(text: String): Boolean{
        return text.isNotEmpty()
    }
}