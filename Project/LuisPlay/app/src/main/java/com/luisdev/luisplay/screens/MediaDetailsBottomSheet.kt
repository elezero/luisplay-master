package com.luisdev.luisplay.screens


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.luisdev.luisplay.apiconsumer.models.MediaBsData
import com.luisdev.luisplay.databinding.BottomSheetMediaDetailsBinding

class MediaDetailsBottomSheet(private val data: MediaBsData) : BottomSheetDialogFragment() {
    lateinit var binding: BottomSheetMediaDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = BottomSheetMediaDetailsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setupUI()
        updateUI()
    }

    private fun setupUI() {
        binding.closeIcon.setOnClickListener { dismiss() }

        val intent = Intent(activity, MediaDetailsActivity::class.java)
        intent.putExtra("id", data.mediaId)
        intent.putExtra("backdropurl", data.posterUrl)
        intent.putExtra("name", data.title)
        intent.putExtra("overview", data.overview)
        intent.putExtra("firstairdate", data.releaseYear)
        intent.putExtra("voteaverage", data.voteAverage)


        binding.detailsButton.setOnClickListener {
            goToMediaDetails(intent)
        }

        binding.playLl.setOnClickListener{
            goToMediaDetails(intent)
        }

        binding.previewLl.setOnClickListener{
            goToMediaDetails(intent)
        }
    }

    private fun goToMediaDetails(intent: Intent){
        startActivity(intent)
        dismiss()
    }

    private fun updateUI() {
        if (data.mediaType == "movie") {
            Glide.with(this).load(data.posterUrl).transform(CenterCrop(), RoundedCorners(8))
                .into(binding.posterImage)
            binding.titleText.text = data.title
            binding.yearText.text = data.releaseYear
            binding.runtimeText.visibility = View.GONE
            binding.overviewText.text = data.overview
        } else if (data.mediaType == "tv") {
            Glide.with(this).load(data.posterUrl).transform(CenterCrop(), RoundedCorners(8))
                .into(binding.posterImage)
            binding.titleText.text = data.title
            binding.yearText.text = data.releaseYear
            binding.runtimeText.visibility = View.GONE
            binding.overviewText.text = data.overview
        }
    }

    companion object {
        fun newInstance(data: MediaBsData): MediaDetailsBottomSheet {
            return MediaDetailsBottomSheet(data)
        }
    }
}
