package com.luisdev.luisplay.screens.fragments


class TVShowsFragment(category: String) : BaseFragment() {
    override var mediaType: String = "tvshows"
    override var mediaCategory: String = category

    override fun fragmentViewCreated() {
    }
}