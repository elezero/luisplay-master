package com.luisdev.luisplay.apiconsumer.services


import android.content.Context
import com.luisdev.luisplay.constants.TMDB_TOKEN
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.lang.Exception

object ApiService {

    private const val BASE_URL = "https://api.themoviedb.org/3/"

    fun getClient(context: Context) : TmdbEndpoints {
        val okHttpClient: OkHttpClient = OkHttpClient.Builder()
            .cache(provideCache(context = context))
            .addInterceptor { chain ->
                val url = chain
                    .request()
                    .url()
                    .newBuilder()
                    .addQueryParameter("api_key", TMDB_TOKEN)
                    .build()
                chain.proceed(chain.request().newBuilder().url(url).build())
            }
            .build()


        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .baseUrl(BASE_URL).build()

        return retrofit.create(TmdbEndpoints::class.java)
    }

    val cacheSize: Long = 10 * 1024 * 1024 // 10 MB

    fun provideCache(context: Context): Cache? {
        var cache: Cache? = null
        try {
            cache = Cache(File(context.cacheDir, "http-cache"), cacheSize)
        } catch (e: Exception) {
            println("Cache Error in creating  Cache!")
        }
        return cache
    }

}