package com.luisdev.luisplay.screens.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.luisdev.luisplay.R
import com.luisdev.luisplay.adapters.PrimitiveAdapter
import com.luisdev.luisplay.constants.IMAGE_BASE_URL
import com.luisdev.luisplay.apiconsumer.models.Media
import com.luisdev.luisplay.apiconsumer.models.MediaResponse
import com.luisdev.luisplay.apiconsumer.services.ApiService
import com.luisdev.luisplay.apiconsumer.services.TmdbEndpoints
import com.luisdev.luisplay.constants.IMAGE_SIZE_NORMAL
import com.luisdev.luisplay.databinding.FragmentFeedBaseBinding
import com.luisdev.luisplay.helpers.getGenresText
import com.luisdev.luisplay.screens.MediaDetailsActivity
import com.luisdev.luisplay.screens.SearchActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class  FeedBaseFragment(mediaType: String) : Fragment() {
    lateinit var binding: FragmentFeedBaseBinding
    lateinit var layoutManagerPopular: GridLayoutManager
    lateinit var layoutManagerTopRated: GridLayoutManager
    lateinit var mediaPopularAdapter: PrimitiveAdapter
    lateinit var mediaTopRatedAdapter: PrimitiveAdapter
    lateinit var mediaListPopular: MutableList<Media>
    lateinit var mediaListTopRated: MutableList<Media>
    lateinit var apiService: TmdbEndpoints/* = ApiService.create()*/

    // ABSTRACT ATTRIBUTE
    open var mediaType: String = mediaType

    open var pagePopular = 0
    open var pageTopRated = 0
    open var isLoadingPopular = false
    open var isLoadingTopRated = false

    open var randomItemIndex: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDataProvider()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentFeedBaseBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupButtonsListeners()
        setUpList()
    }

    open fun setupDataProvider() {
        apiService = ApiService.getClient(context = requireContext())

    }


    open fun setupButtonsListeners() {
        binding.loader.playButton.setOnClickListener {
            goToFeatureDetails()
        }

        binding.loader.infoButton.setOnClickListener {
            goToFeatureDetails()
        }

        binding.mainToolbar.searchIcon.setOnClickListener {
            var intent = Intent(activity, SearchActivity::class.java)
            startActivity(intent)
        }

        binding.btnExplorePopular.setOnClickListener{
            // SHOW Popular fragment
            //requireParentFragment().parentFragmentManager.beginTransaction().hide(this, )
            activity?.supportFragmentManager
                ?.beginTransaction()
                ?.setCustomAnimations(
                    R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right)
                ?.hide(activity?.supportFragmentManager?.findFragmentByTag(mediaType)!!)
                ?.show(activity?.supportFragmentManager?.findFragmentByTag(mediaType+"Popular")!!)
                ?.commit()
        }

        binding.btnExploreToprated.setOnClickListener{

            // SHOW Top Rated fragment
            //requireParentFragment().parentFragmentManager.beginTransaction().hide(this, )
            activity?.supportFragmentManager?.beginTransaction()
                ?.setCustomAnimations(
                    R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right)
                ?.hide(activity?.supportFragmentManager?.findFragmentByTag(mediaType)!!)
                ?.show(activity?.supportFragmentManager?.findFragmentByTag(mediaType+"TopRated")!!)
                ?.commit()

        }
    }

    open fun setUpList() {
        layoutManagerPopular = GridLayoutManager(context, 1, GridLayoutManager.HORIZONTAL, false)
        layoutManagerTopRated = GridLayoutManager(context, 1, GridLayoutManager.HORIZONTAL, false)
        binding.mediaItemListPopular.layoutManager = layoutManagerPopular
        binding.mediaItemListToprated.layoutManager = layoutManagerTopRated


        mediaListPopular = mutableListOf()
        mediaListTopRated = mutableListOf()
        mediaPopularAdapter = PrimitiveAdapter(activity = requireActivity(), mediaList = mediaListPopular)
        mediaTopRatedAdapter = PrimitiveAdapter(activity = requireActivity(), mediaList = mediaListTopRated)

        binding.mediaItemListPopular.adapter = mediaPopularAdapter
        binding.mediaItemListToprated.adapter = mediaTopRatedAdapter
    }

    fun autoScrollPopularList(){
        val linearSnapHelper = LinearSnapHelper()
        linearSnapHelper.attachToRecyclerView(binding.mediaItemListPopular)

        val timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                if (layoutManagerPopular.findLastCompletelyVisibleItemPosition() < mediaPopularAdapter.itemCount - 1) {
                    layoutManagerPopular.smoothScrollToPosition(
                        binding.mediaItemListPopular,
                        RecyclerView.State(),
                        layoutManagerPopular.findLastCompletelyVisibleItemPosition() + 1
                    )
                } else if (layoutManagerPopular.findLastCompletelyVisibleItemPosition() === mediaPopularAdapter.itemCount - 1) {
                    layoutManagerPopular.smoothScrollToPosition(
                        binding.mediaItemListPopular,
                        RecyclerView.State(),
                        0
                    )
                }
            }
        }, 0, 1900)
    }

    fun autoScrollTopRatedList(){
        val linearSnapHelper2 = LinearSnapHelper()
        linearSnapHelper2.attachToRecyclerView(binding.mediaItemListToprated)

        val timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                if (layoutManagerTopRated.findLastCompletelyVisibleItemPosition() < mediaTopRatedAdapter.itemCount - 1) {
                    layoutManagerTopRated.smoothScrollToPosition(
                        binding.mediaItemListToprated,
                        RecyclerView.State(),
                        layoutManagerTopRated.findLastCompletelyVisibleItemPosition() + 1
                    )
                } else if (layoutManagerTopRated.findLastCompletelyVisibleItemPosition() === mediaTopRatedAdapter.itemCount - 1) {
                    layoutManagerTopRated.smoothScrollToPosition(
                        binding.mediaItemListToprated,
                        RecyclerView.State(),
                        0
                    )
                }
            }
        }, 0, 2250)
    }

    fun onFirstDisplay() {
        loadingState(true)
        fetchMediaPopular()
        fetchMediaTopRated()
    }


    open fun updateFeatureMedia() {
        randomItemIndex = (Math.random() * (mediaListPopular.size - 1)).toInt() + 1

        var genres = getGenresText(mediaListPopular[randomItemIndex].genre_ids)

        binding.loader.genresText.text = genres
        Glide.with(binding.loader.backgroundImage)
            .load(IMAGE_BASE_URL + IMAGE_SIZE_NORMAL + mediaListPopular[randomItemIndex].poster_path)
            .into(binding.loader.backgroundImage)
    }

    open fun loadingState(state: Boolean) {
        binding.pgLoading.visibility = if (state) View.VISIBLE else View.GONE
    }

    open fun goToFeatureDetails() {
        if(randomItemIndex != 0){
            val intent = Intent(activity, MediaDetailsActivity::class.java)
            intent.putExtra("id", mediaListPopular[randomItemIndex].id)
            intent.putExtra("backdropurl", IMAGE_BASE_URL + IMAGE_SIZE_NORMAL +mediaListPopular[randomItemIndex].poster_path)
            intent.putExtra("name", mediaListPopular[randomItemIndex].title)
            intent.putExtra("overview", mediaListPopular[randomItemIndex].overview)
            intent.putExtra("firstairdate", mediaListPopular[randomItemIndex].release_date)
            intent.putExtra("voteaverage", mediaListPopular[randomItemIndex].vote_average)
            intent.putExtra("type", mediaType)
            startActivity(intent)
        }
    }

    open fun fetchMediaPopular() {
        isLoadingPopular = true
        ++pagePopular

        var call  = if(mediaType.compareTo("movies") == 0){
            apiService.getPopularMovies(page = pagePopular.toString())
        } else {
            apiService.getPopularTVShows(page = pagePopular.toString())
        }


        call.enqueue(object : Callback<MediaResponse> {
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(call: Call<MediaResponse>?, response: Response<MediaResponse>) {
                if (response.body() != null) {
                    isLoadingPopular = false
                    mediaListPopular.addAll(response.body()!!.results)
                    binding.mediaItemListPopular.adapter?.notifyDataSetChanged()

                    autoScrollPopularList()


                    updateFeatureMedia()
                    loadingState(false)
                }
            }
            override fun onFailure(call: Call<MediaResponse>?, t: Throwable) {
                Toast.makeText(context, "No internet connection", Toast.LENGTH_LONG).show()
                isLoadingPopular = false
            }
        })
    }


    open fun fetchMediaTopRated() {
        isLoadingTopRated = true
        ++pageTopRated

        var call  = if(mediaType.compareTo("movies") == 0){
            apiService.getTopRatedMovies(page = pageTopRated.toString())
        } else {
            apiService.getTopRatedTVShows(page = pageTopRated.toString())
        }


        call.enqueue(object : Callback<MediaResponse> {
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(call: Call<MediaResponse>?, response: Response<MediaResponse>) {
                if (response.body() != null) {
                    isLoadingTopRated = false
                    mediaListTopRated.addAll(response.body()!!.results)
                    binding.mediaItemListToprated.adapter?.notifyDataSetChanged()

                    autoScrollTopRatedList()

                    loadingState(false)
                }
            }
            override fun onFailure(call: Call<MediaResponse>?, t: Throwable) {
                Toast.makeText(context, "No internet connection", Toast.LENGTH_LONG).show()
                isLoadingPopular = false
            }
        })
    }
}