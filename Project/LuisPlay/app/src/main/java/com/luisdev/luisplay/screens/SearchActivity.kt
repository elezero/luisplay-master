package com.luisdev.luisplay.screens

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.GridLayoutManager
import com.luisdev.luisplay.Validator.validateSearchQuery
import com.luisdev.luisplay.adapters.MediaAdapter
import com.luisdev.luisplay.apiconsumer.models.Media
import com.luisdev.luisplay.apiconsumer.models.MediaResponse
import com.luisdev.luisplay.apiconsumer.services.ApiService
import com.luisdev.luisplay.apiconsumer.services.TmdbEndpoints
import com.luisdev.luisplay.databinding.ActivitySearchBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchActivity : BaseActivity() {
    private lateinit var binding: ActivitySearchBinding
    private lateinit var layoutManager: GridLayoutManager
    private lateinit var apiService: TmdbEndpoints
    private lateinit var mediaAdapter: MediaAdapter
    private lateinit var mediaList: MutableList<Media>


    private var textAnt: String = ""
    private var page = 0
    private var isLoading = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(binding.root)



        setupDataProvider()
        setupTextChangedListeners()
        setupButtonListeners()
        setUpList()
        setupScrollListener()
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun setupTextChangedListeners(){
        //binding.toolbar.setNavigationOnClickListener { finish() }
        binding.clearSearchIcon.setOnClickListener {
            binding.searchTextInput.setText("")
        }
        binding.searchTextInput.addTextChangedListener {
            val query = it.toString().trim()
            if(validateSearchQuery(query)){
                page = 0
                fetchMedia()
            } else {
                mediaList.clear()
                binding.mediaItemList.adapter?.notifyDataSetChanged()
            }
        }

        binding.searchTextInput.onFocusChangeListener = View.OnFocusChangeListener { view, hasFocus ->
            run {
                if (!hasFocus) {
                    view.hideKeyboard()
                }
            }
        }
    }

    private fun View.hideKeyboard(){
        val inputMethodManager = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(this.windowToken, 0)
    }


    private fun setupButtonListeners(){
        binding.backButton.setOnClickListener {
            finish()
        }
    }

    private fun setUpList(){
        layoutManager = GridLayoutManager(applicationContext, 3)
        binding.mediaItemList.layoutManager = layoutManager


        mediaList = mutableListOf()
        mediaAdapter = MediaAdapter(activity = this, mediaList = mediaList)

        binding.mediaItemList.adapter = mediaAdapter
    }


    private fun setupDataProvider(){
        apiService = ApiService.getClient(context = applicationContext)
    }

    private fun setupScrollListener(){
        // USING NESTED INSTEAD OF RECYCLERVIEW BECAUSE THERE IS A HEADER
        binding.nestedScrollView.setOnScrollChangeListener { v: NestedScrollView, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            if (v.getChildAt(v.childCount - 1) != null) {
                if (scrollY >= v.getChildAt(v.childCount - 1)
                        .measuredHeight - v.measuredHeight &&
                    scrollY > oldScrollY
                ) {
                    fetchMedia()
                }
            }
        }
    }


    private fun fetchMedia() {
        isLoading = true
        ++page

        val text = binding.searchTextInput.text.toString()

        val call =  apiService.searchMulti(page = page.toString(), query = text)
        call.enqueue(object : Callback<MediaResponse> {
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(call: Call<MediaResponse>?, response: Response<MediaResponse>) {
                //CHECKING VALID RESPONSE
                if (response.body() != null) {
                    if(text.compareTo(textAnt) != 0)
                        mediaList.clear()

                    isLoading = false
                    mediaList.addAll(response.body()!!.results)
                    binding.mediaItemList.adapter?.notifyDataSetChanged()

                    textAnt = text
                }
            }
            override fun onFailure(call: Call<MediaResponse>?, t: Throwable) {
                Toast.makeText(applicationContext, "No internet connection", Toast.LENGTH_LONG).show()
                isLoading = false
            }
        })
    }
}