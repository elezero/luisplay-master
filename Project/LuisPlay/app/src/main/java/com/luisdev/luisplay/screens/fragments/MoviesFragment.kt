package com.luisdev.luisplay.screens.fragments

import com.luisdev.luisplay.screens.MainActivity

class MoviesFragment(category: String) : BaseFragment() {
    override var mediaType: String = "movies"
    override var mediaCategory: String = category

    override fun fragmentViewCreated() {
        (requireActivity() as MainActivity).onMoviesFragmentViewCreated()
    }
}