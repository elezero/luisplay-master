package com.luisdev.luisplay.apiconsumer.models


data class Media(
    val id: Int,
    val title: String, // MOVIE
    val poster_path: String,
    val backdrop_path: String,
    val overview: String,
    val release_date: String, // MOVIE
    val vote_average: Double,
    val genre_ids: List<Int>,
    val first_air_date: String, // TVSHOW
    val name: String, // TVSHOW
    val known_for: List<Media> //OTHER
) {
}