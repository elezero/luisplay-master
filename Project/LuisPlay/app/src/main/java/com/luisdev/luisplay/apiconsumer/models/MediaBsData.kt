package com.luisdev.luisplay.apiconsumer.models


data class MediaBsData(
    val mediaType: String,
    val mediaId: Int,
    val posterUrl: String?,
    val title: String,
    val releaseYear: String?,
    val overview: String,
    val voteAverage: Double
)