package com.luisdev.luisplay.screens.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.luisdev.luisplay.R
import com.luisdev.luisplay.adapters.MediaAdapter
import com.luisdev.luisplay.constants.IMAGE_BASE_URL
import com.luisdev.luisplay.apiconsumer.models.Media
import com.luisdev.luisplay.apiconsumer.models.MediaResponse
import com.luisdev.luisplay.apiconsumer.services.ApiService
import com.luisdev.luisplay.apiconsumer.services.TmdbEndpoints
import com.luisdev.luisplay.constants.IMAGE_SIZE_NORMAL
import com.luisdev.luisplay.databinding.FragmentBaseBinding
import com.luisdev.luisplay.helpers.getGenresText
import com.luisdev.luisplay.screens.MediaDetailsActivity
import com.luisdev.luisplay.screens.SearchActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


abstract class  BaseFragment : Fragment() {
    open lateinit var binding: FragmentBaseBinding
    open lateinit var layoutManager: GridLayoutManager
    open lateinit var mediaAdapter: MediaAdapter
    open lateinit var mediaList: MutableList<Media>
    open lateinit var apiService: TmdbEndpoints/* = ApiService.create()*/

    // ABSTRACT ATTRIBUTE
    abstract var mediaType: String
    abstract var mediaCategory: String

    open var page = 0
    open var isLoading = false

    open var randomItemIndex: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupDataProvider()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        binding = FragmentBaseBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
        setupButtonsListeners()
        setUpList()
        setupScrollListener()
        fragmentViewCreated()
        fetchMedia()
    }

    private fun setupUI(){
        binding.mainToolbar.tbTitle.text = mediaCategory
    }

    open fun setupDataProvider() {
        apiService = ApiService.getClient(context = requireContext())
    }

    open fun setupScrollListener() {
        // USING NESTED INSTEAD OF RECYCLERVIEW BECAUSE THERE IS A HEADER
        binding.nestedScrollView.setOnScrollChangeListener { v: NestedScrollView, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            if (v.getChildAt(v.childCount - 1) != null) {
                if (scrollY >= v.getChildAt(v.childCount - 1)
                        .measuredHeight - v.measuredHeight &&
                    scrollY > oldScrollY
                ) {
                    fetchMedia()
                }
            }
        }
    }

    open fun setupButtonsListeners() {
        binding.loader.playButton.setOnClickListener {
            goToDetails()
        }

        binding.loader.infoButton.setOnClickListener {
            goToDetails()
        }

        binding.mainToolbar.searchIcon.setOnClickListener {
            var intent = Intent(activity, SearchActivity::class.java)
            startActivity(intent)
        }

        binding.mainToolbar.btnBack.setOnClickListener{
            activity?.supportFragmentManager?.beginTransaction()
                ?.setCustomAnimations(
                    R.anim.enter_from_right,
                    R.anim.exit_to_left,
                    R.anim.enter_from_left,
                    R.anim.exit_to_right)
                ?.hide(activity?.supportFragmentManager?.findFragmentByTag(mediaType+mediaCategory)!!)
                ?.show(activity?.supportFragmentManager?.findFragmentByTag(mediaType)!!)
                ?.commit()
        }
    }

    open fun setUpList() {
        layoutManager = GridLayoutManager(context, 3)
        binding.mediaItemList.layoutManager = layoutManager


        mediaList = mutableListOf()
        mediaAdapter = MediaAdapter(activity = requireActivity(), mediaList = mediaList)

        binding.mediaItemList.adapter = mediaAdapter
    }


    fun onFirstDisplay() {
        loadingState(true)
        fetchMedia()
    }


    open fun updateFeatureMedia() {
        randomItemIndex = (Math.random() * (mediaList.size - 1)).toInt() + 1

        var genres = getGenresText(mediaList[randomItemIndex].genre_ids)

        binding.loader.genresText.text = genres
        Glide.with(binding.loader.backgroundImage)
            .load(IMAGE_BASE_URL + IMAGE_SIZE_NORMAL + mediaList[randomItemIndex].poster_path)
            .into(binding.loader.backgroundImage)
    }

    open fun loadingState(state: Boolean) {
        binding.pgLoading.visibility = if (state) View.VISIBLE else View.GONE
    }

    open fun goToDetails() {
        if(randomItemIndex != 0){
            val intent = Intent(activity, MediaDetailsActivity::class.java)
            intent.putExtra("id", mediaList[randomItemIndex].id)
            intent.putExtra("backdropurl", IMAGE_BASE_URL + IMAGE_SIZE_NORMAL +mediaList[randomItemIndex].poster_path)
            intent.putExtra("name", mediaList[randomItemIndex].title)
            intent.putExtra("overview", mediaList[randomItemIndex].overview)
            intent.putExtra("firstairdate", mediaList[randomItemIndex].release_date)
            intent.putExtra("voteaverage", mediaList[randomItemIndex].vote_average)
            intent.putExtra("type", mediaType)
            startActivity(intent)
        }
    }

    open fun fetchMedia() {
        isLoading = true
        ++page

        var call: Call<MediaResponse> = if(mediaType.compareTo("movies") == 0){      // MOVIES
            if(mediaCategory.compareTo("Popular") == 0){
                apiService.getPopularMovies(page = page.toString())
            }else{
                apiService.getTopRatedMovies(page = page.toString())
            }
        } else {                        // TVSHOWS
            if(mediaCategory.compareTo("Popular") == 0){
                apiService.getPopularTVShows(page = page.toString())
            }else{
                apiService.getTopRatedTVShows(page = page.toString())
            }
        }


        call.enqueue(object : Callback<MediaResponse> {
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(call: Call<MediaResponse>?, response: Response<MediaResponse>) {
                if (response.body() != null) {
                    isLoading = false
                    mediaList.addAll(response.body()!!.results)
                    binding.mediaItemList.adapter?.notifyDataSetChanged()

                    updateFeatureMedia()
                    loadingState(false)
                }
            }
            override fun onFailure(call: Call<MediaResponse>?, t: Throwable) {
                Toast.makeText(context, "No internet connection", Toast.LENGTH_LONG).show()
                isLoading = false
            }
        })
    }


    // ABSTRACT METHODS
    abstract fun fragmentViewCreated()

}