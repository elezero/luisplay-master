package com.luisdev.luisplay.apiconsumer.models

data class Genre(val id: Int, val name: String)