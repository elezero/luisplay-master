package com.luisdev.luisplay.apiconsumer.models

data class MediaResponse(val dates: Dates, val page: Int, val results: List<Media>, val total_pages: Int,
                         val total_results: Int) {
}