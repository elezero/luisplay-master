package com.luisdev.luisplay.apiconsumer.models

data class MediaVideoResponse(val id: Int, val results: List<MediaVideo>) {
}