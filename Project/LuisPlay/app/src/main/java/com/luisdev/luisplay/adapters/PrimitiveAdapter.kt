package com.luisdev.luisplay.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.luisdev.luisplay.R
import com.luisdev.luisplay.constants.IMAGE_BASE_URL
import com.luisdev.luisplay.apiconsumer.models.Media
import com.luisdev.luisplay.apiconsumer.models.MediaBsData
import com.luisdev.luisplay.constants.IMAGE_SIZE_NORMAL
import com.luisdev.luisplay.screens.MediaDetailsBottomSheet


class PrimitiveAdapter(private val activity: FragmentActivity, private val mediaList: List<Media>) : RecyclerView.Adapter<PrimitiveAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.primitive_horizontal_card_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mediaItem = mediaList[position]

        // load image
        Glide.with(holder.mediaPicture).load(IMAGE_BASE_URL + IMAGE_SIZE_NORMAL + mediaItem.poster_path)
            .into(holder.mediaPicture)

        holder.itemView.setOnClickListener(View.OnClickListener {
            var ya = false

            var mediaType = ""
            var titleName = ""
            var releaseAirDate = ""
            var posterPath = ""
            var overview = ""
            var voteAverage = 0.0

            // IF OTHER TYPE
            mediaItem.known_for?.let {
                mediaType = "movie"
                ya = true

                if (it.size > 0) {
                    titleName = it[0].title
                    releaseAirDate = it[0].release_date
                    posterPath = it[0].poster_path
                    overview = it[0].overview
                    voteAverage = it[0].vote_average
                } else {
                    titleName = "Unknown"
                    releaseAirDate = "Unknown"
                    posterPath = ""
                    overview = "Unknown"
                    voteAverage = 0.0
                }
            }

            if (!ya) {
                mediaItem.title?.let {
                    mediaType = "movie"
                    titleName = mediaItem.title
                    releaseAirDate = mediaItem.release_date
                }
                mediaItem.title ?: run {
                    mediaType = "tv"
                    titleName = mediaItem.name
                    releaseAirDate = mediaItem.first_air_date
                }

                posterPath = mediaItem.poster_path
                overview = mediaItem.overview
                voteAverage = mediaItem.vote_average
            }


            val mediaBsData = MediaBsData(
                mediaType = mediaType, mediaId = mediaItem.id,
                posterUrl = IMAGE_BASE_URL + IMAGE_SIZE_NORMAL + posterPath, title = titleName,
                releaseYear = releaseAirDate, overview = overview, voteAverage = voteAverage
            )

            MediaDetailsBottomSheet.newInstance(mediaBsData)
                .show(activity.supportFragmentManager, mediaItem.id.toString())
        })
    }

    override fun getItemCount(): Int {
        return mediaList.size
    }

    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val mediaPicture: ImageView = itemView.findViewById(R.id.media_picture)
    }
}