package com.luisdev.luisplay.screens


import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.luisdev.luisplay.apiconsumer.extensions.hide
import com.luisdev.luisplay.apiconsumer.extensions.show
import com.luisdev.luisplay.apiconsumer.models.MediaVideo
import com.luisdev.luisplay.apiconsumer.models.MediaVideoResponse
import com.luisdev.luisplay.apiconsumer.services.ApiService
import com.luisdev.luisplay.apiconsumer.services.TmdbEndpoints
import com.luisdev.luisplay.databinding.ActivityMediaDetailsBinding
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerCallback
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MediaDetailsActivity : BaseActivity() {
    lateinit var binding: ActivityMediaDetailsBinding

    private lateinit var apiService: TmdbEndpoints
    private val pId: Int? get() = intent.extras?.getInt("id")
    private val pBackdropurl: String? get() = intent.extras?.getString("backdropurl")
    private val pName: String? get() = intent.extras?.getString("name")
    private val pOverview: String? get() = intent.extras?.getString("overview")
    private val pFirstAirDate: String? get() = intent.extras?.getString("firstairdate")
    private val pVoteAverage: Double? get() = intent.extras?.getDouble("voteaverage")
    private val type: String? get() = intent.extras?.getString("type")


    var isVideoRestarted = false
    var player: YouTubePlayer? = null
    var bannerVideoLoaded = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMediaDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupDataProvider()
        setupUI()
        updateDetails()
        fetchVideos()
    }

    private fun setupDataProvider() {
        apiService = ApiService.getClient(context = applicationContext)
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.youtubePlayerView.removeYouTubePlayerListener(youTubePlayerListener)

        binding.youtubePlayerView.release()
    }


    private fun setupUI() {
        binding.toolbar.setNavigationOnClickListener { onBackPressed() }
        title = ""
        showBackIcon()

        binding.youtubePlayerView.addYouTubePlayerListener(youTubePlayerListener)
        binding.header.overviewText.setOnClickListener {
            binding.header.overviewText.maxLines = 10
            binding.header.overviewText.isClickable = false
        }


    }

    private fun updateDetails() {
        Glide.with(this).load(pBackdropurl).transform(CenterCrop())
            .into(binding.thumbnail.backdropImage)
        binding.header.titleText.text = pName
        binding.header.overviewText.text = pOverview
        binding.header.yearText.text = pFirstAirDate
        binding.header.runtimeText.visibility = View.GONE
        binding.header.ratingText.text = pVoteAverage.toString()
    }

    private fun checkAndLoadVideo(videos: List<MediaVideo>) {
        val firstVideo =
            videos.firstOrNull { video -> (video.type == "Trailer") && video.site == "YouTube" }
        if (firstVideo != null) {
            if (!bannerVideoLoaded) {
                binding.youtubePlayerView.getYouTubePlayerWhenReady(object : YouTubePlayerCallback {
                    override fun onYouTubePlayer(youTubePlayer: YouTubePlayer) {
                        player = youTubePlayer
                        youTubePlayer.loadVideo(firstVideo.key, 0f)
                        bannerVideoLoaded = true
                    }
                })
            }
        } else {
            binding.thumbnail.playContainer.hide()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private val youTubePlayerListener = object : AbstractYouTubePlayerListener() {
        override fun onCurrentSecond(youTubePlayer: YouTubePlayer, second: Float) {
            if (!isVideoRestarted && second > 3.2) {
                isVideoRestarted = true
                lifecycleScope.launch {
                    youTubePlayer.seekTo(0f)
                    youTubePlayer.unMute()
                    binding.youtubePlayerView.getPlayerUiController().showUi(false)
                    delay(50)
                    binding.thumbnail.container.hide()
                    binding.thumbnail.videoLoader.hide()
                    binding.youtubePlayerView.show()
                    delay(1000)
                    binding.youtubePlayerView.getPlayerUiController().showUi(true)
                }
            }
        }

        override fun onStateChange(
            youTubePlayer: YouTubePlayer,
            state: PlayerConstants.PlayerState,
        ) {
            if (!isVideoRestarted) {
                youTubePlayer.mute()
            }

            if (state == PlayerConstants.PlayerState.ENDED) {
                binding.youtubePlayerView.hide()
                binding.thumbnail.container.show()
                binding.thumbnail.videoLoader.hide()
                youTubePlayer.pause()
            }
        }
    }

    private fun fetchVideos() {

        val call: Call<MediaVideoResponse> = when (type) {
            "movies" -> apiService.getMovieVideo(movieId = pId!!)
            "tvshows" -> apiService.getTVShowVideo(tvId = pId!!)
            else -> apiService.getMovieVideo(movieId = pId!!)
        }

        call.enqueue(object : Callback<MediaVideoResponse> {
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<MediaVideoResponse>?,
                response: Response<MediaVideoResponse>
            ) {
                if (response.body() != null) {
                    checkAndLoadVideo(videos = response.body()!!.results)
                }
            }

            override fun onFailure(call: Call<MediaVideoResponse>?, t: Throwable) {
                Toast.makeText(applicationContext, "No internet connection", Toast.LENGTH_LONG).show()
            }
        })
    }
}
