package com.luisdev.luisplay.screens


import android.os.Bundle
import androidx.fragment.app.Fragment
import com.luisdev.luisplay.R
import com.luisdev.luisplay.databinding.ActivityMainBinding
import com.luisdev.luisplay.helpers.checkInternetConnection
import com.luisdev.luisplay.screens.fragments.FeedBaseFragment
import com.luisdev.luisplay.screens.fragments.MoviesFragment
import com.luisdev.luisplay.screens.fragments.TVShowsFragment


class MainActivity : BaseActivity() {
    private lateinit var binding: ActivityMainBinding

    // Flags to know whether bottom tab fragments are displayed at least once
    private val fragmentFirstDisplay = mutableListOf(false, false)

    private val moviesFragment = FeedBaseFragment("movies")
    private val moviesPopularFragment = MoviesFragment("Popular")
    private val moviesTopRatedFragment = MoviesFragment("TopRated")

    private val tvShowsFragment = FeedBaseFragment("tvshows")
    private val tvShowsPopularFragment = TVShowsFragment("Popular")
    private val tvShowsTopRatedFragment = TVShowsFragment("TopRated")

    private val fragmentManager = supportFragmentManager
    private var activeFragment: Fragment = moviesPopularFragment


    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_LuisPlay)
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setupUI()
        checkInternetConnection(applicationContext)
    }


    private fun setupUI() {
        fragmentManager.beginTransaction().apply {
            add(R.id.container, moviesFragment, "movies")
            add(R.id.container, moviesPopularFragment, "moviesPopular").hide(moviesPopularFragment)
            add(R.id.container, moviesTopRatedFragment, "moviesTopRated").hide(moviesTopRatedFragment)
            add(R.id.container, tvShowsFragment, "tvshows").hide(tvShowsFragment)
            add(R.id.container, tvShowsPopularFragment, "tvshowsPopular").hide(tvShowsPopularFragment)
            add(R.id.container, tvShowsTopRatedFragment, "tvshowsTopRated").hide(tvShowsTopRatedFragment)
        }.commit()


        binding.bottomNavView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.movies_action -> {
                    fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_right)
                        .hide(activeFragment)
                        .hide(moviesPopularFragment)
                        .hide(moviesTopRatedFragment)
                        .hide(tvShowsFragment)
                        .hide(tvShowsPopularFragment)
                        .hide(tvShowsTopRatedFragment)
                        .show(moviesFragment).commit()
                    activeFragment = moviesFragment

                    true
                }
                R.id.tvshows_action -> {
                    if (!fragmentFirstDisplay[1]) {
                        fragmentFirstDisplay[1] = true
                        tvShowsFragment.onFirstDisplay()
                    }
                    fragmentManager.beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_left, R.anim.enter_from_right, R.anim.exit_to_right)
                        .hide(activeFragment)
                        .hide(moviesPopularFragment)
                        .hide(moviesTopRatedFragment)
                        .hide(moviesFragment)
                        .hide(tvShowsPopularFragment)
                        .hide(tvShowsTopRatedFragment)
                        .show(tvShowsFragment).commit()
                    activeFragment = tvShowsFragment

                    true
                }

                else -> false
            }
        }
    }


    fun onMoviesFragmentViewCreated() {
        if (!fragmentFirstDisplay[0]) {
            fragmentFirstDisplay[0] = true
            moviesFragment.onFirstDisplay()
        }
    }
}