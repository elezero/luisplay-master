package com.luisdev.luisplay.apiconsumer.services

import com.luisdev.luisplay.apiconsumer.models.MediaResponse
import com.luisdev.luisplay.apiconsumer.models.MediaVideoResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TmdbEndpoints {
    // --------------------------  ENDPOINTS ----------------------------
    /* MOVIES */
    @GET("movie/popular")
    fun getPopularMovies(
        @Query("page") page: String
    ) : Call<MediaResponse>

    @GET("movie/top_rated")
    fun getTopRatedMovies(
        @Query("page") page: String
    ) : Call<MediaResponse>

    @GET("movie/{movieId}/videos")
    fun getMovieVideo(
        @Path("movieId") movieId: Int
    ): Call<MediaVideoResponse>


    /* TV SHOWS */
    @GET("tv/popular")
    fun getPopularTVShows(
        @Query("page") page: String
    ) : Call<MediaResponse>


    @GET("tv/top_rated")
    fun getTopRatedTVShows(
        @Query("page") page: String
    ) : Call<MediaResponse>

    @GET("tv/{tvId}/videos")
    fun getTVShowVideo(
        @Path("tvId") tvId: Int
    ): Call<MediaVideoResponse>


    // ----- MULTI
    @GET("search/multi")
    fun searchMulti(
        @Query("query") query: String,
        @Query("page") page: String
    ): Call<MediaResponse>
}