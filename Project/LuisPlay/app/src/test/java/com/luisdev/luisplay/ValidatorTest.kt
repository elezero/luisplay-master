package com.luisdev.luisplay

import com.google.common.truth.Truth.assertThat
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class ValidatorTest {

    /**
     * PRUEBAS UNITARIAS PARA EL UNICO INPUT POR PARTE DEL USUARIO
     *      -> Search Input
     */
    @Test
    fun whenInputIsValid(){
        val searchText = "Some text"

        val result = Validator.validateSearchQuery(searchText)

        assertThat(result).isEqualTo(true)
    }

    @Test
    fun whenInputIsInvalid(){
        val searchText = ""

        val result = Validator.validateSearchQuery(searchText)

        assertThat(result).isEqualTo(false)
    }
}