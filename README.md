# README #

Luis Play is an Android Mobile App coded in Kotlin using libreries to show data from The Movie DB API.

### How do I get set up? ###
Follow configuration guideline


### Technologies and Libraries ###

* Android Studio
* Kotlin
* Glide
* Retrofit
* JUnit for Unit Testing
* Retrofit Converter 
* Android Youtube Player for trailer preview in media details
* Truth
* Recycler Views
* Adapters
* Animations
* Helpers

### Configuration ###
Open File com.luisdev.luisplay/constants/constans.kt
Change line 4 TMDB_TOKEN by your The Movie DB API Token

### Creator ###

* Owner Luis Quijada Figueroa <jose.jlq@hotmail.com>